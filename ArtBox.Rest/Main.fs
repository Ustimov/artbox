namespace ArtBox.REST

open System
open ArtBox.REST.DTO
open ArtBox.REST.Models
open WebSharper
open WebSharper.Sitelets

type EndPoint =
    | [<EndPoint "/">] Home
    | [<EndPoint "POST /register"; Json "info">] Register of info: RegisterDTO
    | [<EndPoint "POST /auth"; Json "credentials">] Auth of credentials: AuthDTO
    | [<EndPoint "POST /logout">] Logout
    | [<EndPoint "GET /folder">] FolderInfo of int64
    | [<EndPoint "POST /file"; Json "info">] CreateFile of info: FileDTO
    | [<EndPoint "POST /upload">] UploadFile
    | [<EndPoint "GET /file">] FileInfo of int64
    | [<EndPoint "POST /folder"; Json "info">] CreateFolder of info: FolderDTO
    | [<EndPoint "GET /box">] BoxRoot
    | [<EndPoint "GET /box">] Box of int64
    | [<EndPoint "PUT /file"; Json "info">] UpdateFile of info: FileModel
    | [<EndPoint "PUT /folder"; Json "info">] UpdateFolder of info: FolderModel
    | [<EndPoint "DELETE /file">] DeleteFile of int64
    | [<EndPoint "DELETE /folder">] DeleteFolder of int64
    | [<EndPoint "POST /fullsearch"; Json "info">] FullSearch of info: FullSearchDTO
    | [<EndPoint "POST /search"; Json "info">] ShortSearch of info: ShortSearchDTO
    | [<EndPoint "GET /download">] DownloadFile of int64
    | [<EndPoint "POST /changepassword"; Json "info">] ChangePassword of info: ChangePasswordDTO
    | [<EndPoint "POST /uploadphoto">] UploadPhoto
    | [<EndPoint "POST /profile"; Json "info">] UpdateProfile of info: ProfileDTO
    | [<EndPoint "GET /profile">] Profile
    | [<EndPoint "GET /photo">] Photo

module Site =
    open ArtBox.Core.Services
    open ArtBox.REST.Adapters
    open ArtBox.REST.Services
    open System
    open System.IO
    open System.Web
    
    type WebService() =

        let userService = UserService()
        let folderService: IFolderService = FolderServiceAdapter(FolderService()) :> IFolderService
        let fileService: IFileService = FileServiceAdapter(FileService()) :> IFileService

        let emptyDescription = ""
        let filesDir = "C:\Users\Ustimov\Documents\Visual Studio 2015\Projects\ArtBox\Files"

        let boxForFolder folderId = 
            let folders = folderService.GetFoldersForParent(folderId)
            let files = fileService.GetFilesForFolder(folderId)
            { Id = folderId; Folders = folders; Files = files }

        let convertDates(dates: string option[]) = 
            let result: DateTime option[] = Array.zeroCreate dates.Length
            for i in 0..dates.Length - 1 do
                result.[i] <- match dates.[i] with
                                | None -> None
                                | Some(date) ->
                                    let ok, parsedDate = DateTime.TryParse(date)
                                    if ok then Some(parsedDate) else None
            result

        let generatePath (file: HttpPostedFileBase) = 
            let c = Directory.GetCurrentDirectory()
            if not (Directory.Exists(filesDir)) then Directory.CreateDirectory(filesDir) |> ignore
            sprintf "%s/%s%s" filesDir (DateTime.Now.ToString("ddMMyyyy")) file.FileName

        member this.Home(ctx) =
            async {
                let! user = ctx.UserSession.GetLoggedInUser()
                match user with
                | None -> return! Content.Json { Error = "Ok"; Result = Some(false) }
                | Some(email) -> return! Content.Json { Error = "Ok"; Result = Some(true) }
            }

        member this.Register(ctx, registerDTO: RegisterDTO) =
            let _, msg = userService.Register(registerDTO.Email, registerDTO.Password, registerDTO.FullName)
            Content.Json { Error = msg; Result = None }

        member this.Auth(ctx, authDTO: AuthDTO) =
            async {
                let ok, msg = userService.Auth(authDTO.Email, authDTO.Password)
                if ok then do! ctx.UserSession.LoginUser authDTO.Email
                return! Content.Json { Error = msg; Result = None }
            }
        
        member this.Logout(ctx) = 
            async {
                do! ctx.UserSession.Logout()
                return! Content.Json { Error = "Ok"; Result = None }
            }
        
        member this.FolderInfo(ctx, folderId) = 
            async {
                let! user = ctx.UserSession.GetLoggedInUser()
                match user with
                | None -> return! Content.Json { Error = "Not authorized"; Result = None }
                | Some(email) ->
                    let ok, msg, folder = folderService.GetFolderById(email, folderId)
                    if ok then
                        return! Content.Json { Error = msg; Result = folder }
                    else
                        return! Content.Json { Error = msg; Result = None }
            }

        member this.CreateFile(ctx, fileDTO: FileDTO) = 
            async {
                let! email = ctx.UserSession.GetLoggedInUser()
                match email with
                | None -> return! Content.Json { Error = "Not authorized"; Result = None }
                | Some(user) ->
                    match fileDTO.Id with
                    | None -> return! Content.Json { Error = "You should provide file id"; Result = None }
                    | Some(id) ->
                        let _, msg = fileService.UpdateFile(user, id, fileDTO.Name, fileDTO.Access, fileDTO.Description, fileDTO.Folder)
                        return! Content.Json { Error = msg; Result = None }
            }

        member this.FileInfo(ctx, fileId) = 
            async {
                let! email = ctx.UserSession.GetLoggedInUser()
                match email with
                | None -> return! Content.Json { Error = "Not authorized"; Result = None }
                | Some(user) ->
                    let ok, msg, file = fileService.GetFileById(user, fileId)
                    if ok then return! Content.Json { Error = msg; Result = file }
                    else return! Content.Json { Error = msg; Result = None }
            }

        member this.CreateFolder(ctx, folderDTO: FolderDTO) = 
            async {
                let! email = ctx.UserSession.GetLoggedInUser()
                match email with
                | None -> return! Content.Json { Error = "Not authorized"; Result = None }
                | Some(user) ->
                    let _, msg = folderService.CreateFolder(folderDTO.Name, folderDTO.Parent, folderDTO.Description, folderDTO.Access, user)
                    return! Content.Json { Error = msg; Result = None }
            }

        member this.BoxRoot(ctx) = 
            async {
                let! email = ctx.UserSession.GetLoggedInUser()
                match email with
                | None -> return! Content.Json { Error = "Not authorized"; Result = None }
                | Some(user) ->
                    let ok, msg, folder = folderService.GetUserRootFolder(user)
                    if not ok then return! Content.Json { Error = msg; Result = None }
                    else return! Content.Json { Error = msg; Result = Some(boxForFolder folder.Value.Id) }
            }

        member this.Box(ctx, folderId) =
            async {
                let! email = ctx.UserSession.GetLoggedInUser()
                match email with
                | None -> return! Content.Json { Error = "Not authorized"; Result = None }
                | Some(user) ->
                    return! Content.Json { Error = "Ok"; Result = Some(boxForFolder folderId) }
            }

        member this.UpdateFile(ctx, fileModel: FileModel) = 
            async {
                let! email = ctx.UserSession.GetLoggedInUser()
                match email with
                | None -> return! Content.Json { Error = "Not authorized"; Result = None }
                | Some(user) ->
                    let _, msg = fileService.UpdateFile(user, fileModel.Id, fileModel.Name, fileModel.Access, fileModel.Description, fileModel.Folder)
                    return! Content.Json { Error = msg; Result = None }
            }

        member this.UpdateFolder(ctx, folderModel: FolderModel) = 
            async {
                let! email = ctx.UserSession.GetLoggedInUser()
                match email with
                | None -> return! Content.Json { Error = "Not authorized"; Result = None }
                | Some(user) ->
                    let _, msg = folderService.UpdateFolder(user, folderModel.Id, folderModel.Name, folderModel.Access, folderModel.Description)
                    return! Content.Json { Error = msg; Result = None }
            }

        member this.DeleteFile(ctx, fileId) =
            async {
                let! email = ctx.UserSession.GetLoggedInUser()
                match email with
                | None -> return! Content.Json { Error = "Not authorized"; Result = None }
                | Some(user) ->
                    let _, msg = fileService.DeleteFile(user, fileId)
                    return! Content.Json { Error = msg; Result = None }
            }

        member this.DeleteFolder(ctx, folderId) = 
            async {
                let! email = ctx.UserSession.GetLoggedInUser()
                match email with
                | None -> return! Content.Json { Error = "Not authorized"; Result = None }
                | Some(user) ->
                    let _, msg = folderService.DeleteFolder(user, folderId)
                    return! Content.Json { Error = msg; Result = None }
            }

        member this.ShortSearch(ctx, shortSearchDTO: ShortSearchDTO) = 
            async {
                let! email = ctx.UserSession.GetLoggedInUser()
                match email with
                | None -> return! Content.Json { Error = "Not authorized"; Result = None }
                | Some(user) ->
                    let folders = folderService.ShortSearch(shortSearchDTO.Query, user)
                    let files = fileService.ShortSearch(shortSearchDTO.Query, user)
                    return! Content.Json { Error = "Ok"; Result = Some({ Id = -1L; Files = files; Folders = folders }) }
            }

        member this.FullSearch(ctx, fullSearchDTO: FullSearchDTO) = 
            async {
                let! email = ctx.UserSession.GetLoggedInUser()
                match email with
                | None -> return! Content.Json { Error = "Not authorized"; Result = None }
                | Some(user) ->
                    let dates = convertDates([|fullSearchDTO.DateCreatedFrom; fullSearchDTO.DateCreatedTo; fullSearchDTO.DateChangedFrom; fullSearchDTO.DateChangedTo|])
                    let files = fileService.FullSearch(user, fullSearchDTO.Query, fullSearchDTO.InName, fullSearchDTO.InDescription, fullSearchDTO.InType, dates.[0], dates.[1], dates.[2], dates.[3])
                    return! Content.Json { Error = "Ok"; Result = Some({ Id = -1L; Files = files; Folders = [||] }) }
            }

        member this.UploadFile(ctx) = 
            async {
                let! email = ctx.UserSession.GetLoggedInUser()
                match email with
                | None -> return! Content.Json { Error = "Not authorized"; Result = None }
                | Some(user) ->
                    let files = ctx.Request.Files |> Seq.toArray
                    if files.Length <> 1 then
                        return! Content.Json { Error = "Incorrect file count"; Result = None }
                    else
                        let path = generatePath files.[0]
                        files.[0].SaveAs(path)
                        let ok, msg, folder = folderService.GetUserRootFolder(user)
                        if not ok then
                            return! Content.Json { Error = msg; Result = None }
                        else
                            let id, msg = fileService.SaveFile(files.[0].FileName, folder.Value.Id, files.[0].ContentType, int64(files.[0].ContentLength), 
                                                                int64(AccessMode.Private), emptyDescription, user, path)
                            return! Content.Json { Error = msg; Result = id }
            }

        member this.DownloadFile(ctx, fileId) = 
            async {
                let! email = ctx.UserSession.GetLoggedInUser()
                match email with
                | None -> return! Content.Json { Error = "Not authorized"; Result = None }
                | Some(user) ->
                    let ok, msg, file = fileService.GetFileById(user, fileId)
                    if not ok then
                        return! Content.Json { Error = msg; Result = None }
                    else
                        match file with
                        | None -> return! Content.Json { Error = "File not found"; Result = None }
                        | Some(f) ->
                            return! Content.File(f.Path, true, f.Type)
            }

        member this.ChangePassword(ctx, changePasswordDTO: ChangePasswordDTO) = 
            async {
                let! email = ctx.UserSession.GetLoggedInUser()
                match email with
                | None -> return! Content.Json { Error = "Not authorized"; Result = None }
                | Some(user) ->
                    let _, msg = userService.UpdatePassword(user, changePasswordDTO.OldPassword, changePasswordDTO.Password)
                    return! Content.Json { Error = msg; Result = None }
            }

        member this.UploadPhoto(ctx) = 
            async {
                let! email = ctx.UserSession.GetLoggedInUser()
                match email with
                | None -> return! Content.Json { Error = "Not authorized"; Result = None }
                | Some(user) ->
                    let files = ctx.Request.Files |> Seq.toArray
                    if files.Length <> 1 then
                        return! Content.Json { Error = "Incorrect file count"; Result = None }
                    else
                        let path = generatePath files.[0]
                        files.[0].SaveAs(path)
                        let _, msg = userService.UpdatePhoto(user, path)
                        return! Content.Json { Error = msg; Result = None }
            }

        member this.Photo(ctx) = 
            async {
                let! email = ctx.UserSession.GetLoggedInUser()
                match email with
                | None -> return! Content.Json { Error = "Not authorized"; Result = None }
                | Some(user) ->
                    let ok, msg, path = userService.GetPhotoPath(user)
                    if not ok then
                        return! Content.Json { Error = msg; Result = None }
                    else
                        return! Content.File(path, true, MimeMapping.GetMimeMapping(path))
            }

        member this.Profile(ctx) = 
            async {
                let! email = ctx.UserSession.GetLoggedInUser()
                match email with
                | None -> return! Content.Json { Error = "Not authorized"; Result = None }
                | Some(user) ->
                    let ok, msg, fullName = userService.GetFullName(user)
                    if not ok then
                        return! Content.Json { Error = msg; Result = None }
                    else
                        return! Content.Json { Error = msg; Result = Some({ FullName = fullName }) }
            }

        member this.UpdateProfile(ctx, profileDTO: ProfileDTO) = 
            async {
                let! email = ctx.UserSession.GetLoggedInUser()
                match email with
                | None -> return! Content.Json { Error = "Not authorized"; Result = None }
                | Some(user) ->
                    let ok, msg = userService.UpdateFullName(user, profileDTO.FullName)
                    return! Content.Json { Error = msg; Result = None }
            }

    [<Website>]
    let Main =
        let webService = WebService()
        WebSharper.Web.Remoting.AddAllowedOrigin "http://localhost:4200"
        Application.MultiPage (fun ctx endpoint ->
            match endpoint with
            | EndPoint.Home -> webService.Home(ctx)
            | EndPoint.Register registerDTO -> webService.Register(ctx, registerDTO)
            | EndPoint.Auth authDTO -> webService.Auth(ctx, authDTO)
            | EndPoint.Logout -> webService.Logout(ctx)
            | EndPoint.FolderInfo folderId -> webService.FolderInfo(ctx, folderId)
            | EndPoint.CreateFile fileDTO -> webService.CreateFile(ctx, fileDTO)
            | EndPoint.FileInfo fileId -> webService.FileInfo(ctx, fileId)
            | EndPoint.CreateFolder folderDTO -> webService.CreateFolder(ctx, folderDTO)
            | EndPoint.BoxRoot -> webService.BoxRoot(ctx)
            | EndPoint.Box folderId -> webService.Box(ctx, folderId)
            | EndPoint.UpdateFile fileModel -> webService.UpdateFile(ctx, fileModel)
            | EndPoint.UpdateFolder folderModel -> webService.UpdateFolder(ctx, folderModel)
            | EndPoint.DeleteFile fileId -> webService.DeleteFile(ctx, fileId)
            | EndPoint.DeleteFolder folderId -> webService.DeleteFolder(ctx, folderId)
            | EndPoint.ShortSearch shortSearchDTO -> webService.ShortSearch(ctx, shortSearchDTO)
            | EndPoint.FullSearch fullSearchDTO -> webService.FullSearch(ctx, fullSearchDTO)
            | EndPoint.UploadFile -> webService.UploadFile(ctx)
            | EndPoint.DownloadFile fileId -> webService.DownloadFile(ctx, fileId)
            | EndPoint.ChangePassword changePasswordDTO -> webService.ChangePassword(ctx, changePasswordDTO)
            | EndPoint.UploadPhoto -> webService.UploadPhoto(ctx)
            | EndPoint.Photo -> webService.Photo(ctx)
            | EndPoint.Profile -> webService.Profile(ctx)
            | EndPoint.UpdateProfile profileDTO -> webService.UpdateProfile(ctx, profileDTO)
        )
