namespace ArtBox.REST.DTO

open System
open ArtBox.REST.Models

type ResponseDTO<'T> =
    { Error: string
      Result: 'T option }

type RegisterDTO = 
    { Email: string
      Password: string
      FullName: string }

type AuthDTO =
    { Email: string
      Password: string }

type FileDTO = 
    { Id: int64 option
      Name: string
      Folder: int64
      Description: string
      Access: int64 }

type FolderDTO = 
    { Name: string
      Parent: int64
      Description: string
      Access: int64 }

type BoxDTO =
    { Id: int64
      Folders: FolderModel[]
      Files: FileModel[] }

type ShortSearchDTO = 
    { Query: string }

type FullSearchDTO = 
    { Query: string option
      InName: bool option
      InDescription: bool option
      InType: bool option
      DateCreatedFrom: string option
      DateCreatedTo: string option
      DateChangedFrom: string option
      DateChangedTo: string option }

type ChangePasswordDTO =
    { OldPassword: string
      Password: string }

type ProfileDTO =
    { FullName: string }