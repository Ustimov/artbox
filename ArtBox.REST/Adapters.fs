namespace ArtBox.REST.Adapters

open ArtBox.Core.Services
open ArtBox.REST.Models
open ArtBox.REST.Services
open System

type FolderServiceAdapter(folderService: FolderService) =

    let mapFolder folderTuple = 
        let id, name, parent, dateCreated, access, description, user = folderTuple
        { Id = id; Name = name; Parent = parent; DateCreated = dateCreated; Access = access; Description = description; User = user }

    interface IFolderService with

        member this.GetUserRootFolder(email) =
            let ok, msg, folderTuple = folderService.GetUserRootFolder(email)
            if folderTuple.IsNone then (ok, msg, None)
            else (ok, msg, Some(mapFolder folderTuple.Value))

        member this.GetFolderById(email, id) = 
            let ok, msg, folderTuple = folderService.GetFolderById(email, id)
            if folderTuple.IsNone then (ok, msg, None)
            else (ok, msg, Some(mapFolder folderTuple.Value))

        member this.CreateFolder(name, parent, description, access, email) = 
            folderService.CreateFolder(name, parent, description, access, email)

        member this.GetFoldersForParent(parent) = 
            folderService.GetFoldersForParent(parent)
            |> Seq.map (fun f -> mapFolder f)
            |> Seq.toArray

        member this.UpdateFolder(email, id, name, access, description) = 
            folderService.UpdateFolder(email, id, name, access, description)

        member this.DeleteFolder(email, id) = 
            folderService.DeleteFolder(email, id)

        member this.ShortSearch(query, user) = 
            folderService.ShortSearch(query, user)
            |> Seq.map (fun f -> mapFolder f)
            |> Seq.toArray

type FileServiceAdapter(fileService: FileService) = 

    let mapFile fileTuple = 
        let id, name, folder, dateCreated, dateChanged, fileType, size, access, description, user, path = fileTuple
        { Id = id; Name = name; Folder = folder; DateCreated = dateCreated; DateChanged = dateChanged; Type = fileType; Size = size; Access = access; Description = description; User = user; Path = path; }

    interface IFileService with

        member this.SaveFile(name, folder, fileType, size, access, description, user, path) =
            fileService.SaveFile(name, folder, fileType, size, access, description, user, path)

        member this.GetFileById(email, id) = 
            let ok, msg, fileTuple = fileService.GetFileById(email, id)
            if fileTuple.IsNone then (ok, msg, None)
            else (ok, msg, Some(mapFile fileTuple.Value))

        member this.GetFilesForUser(user) = 
            fileService.GetFilesForUser(user)
            |> Seq.map (fun f -> mapFile f)
            |> Seq.toArray

        member this.GetFilesForFolder(folder) = 
            fileService.GetFilesForFolder(folder)
            |> Seq.map (fun f -> mapFile f)
            |> Seq.toArray

        member this.UpdateFile(email, id, name, access, description, folder) = 
            fileService.UpdateFile(email, id, name, access, description, folder)

        member this.DeleteFile(email, id) =
            fileService.DeleteFile(email, id)

        member this.ShortSearch(query, user) = 
            fileService.ShortSearch(query, user)
            |> Seq.map (fun f -> mapFile f)
            |> Seq.toArray

        member this.FullSearch(email, query: string option, inName: bool option, inDescription: bool option,
                               inType: bool option, dateCreatedFrom: DateTime option, dateCreatedTo: DateTime option,
                               dateChangedFrom: DateTime option, dateChangedTo: DateTime option) =
            fileService.FullSearch(email, query, inName, inDescription, inType, dateCreatedFrom,
                                   dateCreatedTo, dateChangedFrom, dateChangedTo)
            |> Seq.map (fun f -> mapFile f)
            |> Seq.toArray
