namespace ArtBox.REST.Services

open ArtBox.REST.Models
open System

type IFolderService =
    abstract member GetUserRootFolder: string -> bool * string * FolderModel option
    abstract member GetFolderById: string * int64 -> bool * string * FolderModel option 
    abstract member CreateFolder: string * int64 * string * int64 * string -> bool * string
    abstract member GetFoldersForParent: int64 -> FolderModel[]
    abstract member UpdateFolder: string * int64 * string * int64 * string -> bool * string
    abstract member DeleteFolder: string * int64 -> bool * string
    abstract member ShortSearch: string * string -> FolderModel[]

type IFileService = 
    abstract member SaveFile: string * int64 * string * int64 * int64 * string * string * string -> int64 option * string
    abstract member GetFileById: string * int64 -> bool * string * FileModel option
    abstract member GetFilesForUser: string -> FileModel[]
    abstract member GetFilesForFolder: int64 -> FileModel[]
    abstract member UpdateFile: string * int64 * string * int64 * string * int64 -> bool * string
    abstract member DeleteFile: string * int64 -> bool * string
    abstract member ShortSearch: string * string -> FileModel[]
    abstract member FullSearch: string * string option * bool option * bool option * bool option * DateTime option * DateTime option * DateTime option * DateTime option -> FileModel[]
