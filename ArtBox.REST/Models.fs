namespace ArtBox.REST.Models

type AccessMode =
    | Private = 1L
    | Public = 2L

type FolderModel = 
    { Id: int64
      Name: string
      Parent: int64
      DateCreated: int64
      Access: int64
      Description: string
      User: string }

type FileModel =
    { Id: int64
      Name: string
      Folder: int64
      DateCreated: string
      DateChanged: string
      Type: string
      Size: int64
      Access: int64
      Description: string
      User: string
      Path: string }