﻿namespace ArtBox.Core.Gateways

open ArtBox.Core.Database
open System.Linq
open System

type UserTableDataGateway() = 

    let ctx = Sql.GetDataContext()

    let firstUser (users: IQueryable<Sql.dataContext.``main.UserEntity``>) = 
        users
        |> Seq.map (fun u -> (u.Email, u.Password, u.FullName, u.Photo))
        |> Seq.tryHead

    let userWithEmail email = query { for u in ctx.Main.User do where (u.Email = email) }

    member this.Insert(email, password, fullName, photo) = 
        let user = ctx.Main.User.Create()
        user.Email <- email
        user.Password <- password
        user.FullName <- fullName
        user.Photo <- photo
        ctx.SubmitUpdatesAsync() |> Async.StartAsTask

    member this.FindByEmail(email) =
        query { for u in ctx.Main.User do where (u.Email = email) }
        |> firstUser

    member this.Update(email, password, fullName, photo) = 
        let user = email |> userWithEmail |> Seq.tryHead
        match user with
        | None -> failwith "User not found"
        | Some(u) -> u.Password <- password
                     u.FullName <- fullName
                     u.Photo <- photo
                     ctx.SubmitUpdatesAsync() |> Async.StartAsTask

    member this.Delete(email) = 
        let user = email |> userWithEmail |> Seq.tryHead
        match user with
        | None -> failwith "User not found"
        | Some(u) -> u.Delete()
                     ctx.SubmitUpdatesAsync() |> Async.StartAsTask

type FolderTableDataGateway() = 

    let ctx = Sql.GetDataContext()

    let mapFolders (folders: seq<Sql.dataContext.``main.FolderEntity``>) = 
        folders
        |> Seq.map (fun f -> (f.Id, f.Name, f.Parent, f.DateCreated, f.Access, f.Description, f.User))

    member this.Insert(name, parent, access, description, user) = 
        let folder = ctx.Main.Folder.Create()
        folder.Name <- name
        folder.Parent <- parent
        folder.Access <- access
        folder.Description <- description
        folder.DateCreated <- DateTime.Now.ToFileTimeUtc()
        folder.User <- user
        ctx.SubmitUpdatesAsync() |> Async.StartAsTask

    member this.Find(id) =
        query { for f in ctx.Main.Folder do where (f.Id = id) }
        |> mapFolders
        |> Seq.tryHead

    member this.FindUserRoot(email) = 
        query { for f in ctx.Main.Folder do where (f.User = email && f.Name = "Root") }
        |> mapFolders
        |> Seq.tryHead

    member this.FindForUser(user) = 
        query { for f in ctx.Main.Folder do where (f.User = user) }
        |> mapFolders

    member this.FindForParent(parent) =
        query { for f in ctx.Main.Folder do where (f.Parent = parent) }
        |> mapFolders

    member this.Update(id, name, access, description, user) =
        let folder = query { for f in ctx.Main.Folder do where (f.Id = id) } |> Seq.tryHead
        match folder with
        | None -> failwith "Folder not found"
        | Some(f) ->
            f.Name <- name
            f.Access <- access
            f.Description <- description
            ctx.SubmitUpdatesAsync() |> Async.StartAsTask

    member this.Delete(id) =
        let folder = query { for f in ctx.Main.Folder do where (f.Id = id) } |> Seq.tryHead
        match folder with
        | None -> failwith "Folder not found"
        | Some(f) ->
            f.Delete()
            ctx.SubmitUpdatesAsync() |> Async.StartAsTask

    member this.FindWithNameContains(q: string, user) = 
        let upperQuery = q.ToUpper()
        query { for f in ctx.Main.Folder do where (f.User = user) }
        |> Seq.filter (fun f -> f.Name.ToUpper().Contains(upperQuery))
        |> mapFolders

    member this.FindAll() = 
        ctx.Main.Folder
        |> mapFolders

type FileTableDataGateway() = 

    let ctx = Sql.GetDataContext()

    let mapFiles (files: seq<Sql.dataContext.``main.FileEntity``>) = 
        files
        |> Seq.map (fun f -> (f.Id, f.Name, f.Folder, f.DateCreated, f.DateChanged, f.Type, f.Size, f.Access, f.Description, f.User, f.Path))

    member this.Insert(name, folder, fileType, size, access, description, user, path) =
        let file = ctx.Main.File.Create()
        file.Name <- name
        file.Folder <- folder
        file.DateCreated <- DateTime.Now.ToString()
        file.DateChanged <- DateTime.Now.ToString()
        file.Type <- fileType
        file.Size <- size
        file.Access <- access
        file.Description <- description
        file.User <- user
        file.Path <- path
        ctx.SubmitUpdates()
        file.Id
    
    member this.FindForFolder(folderId) =
        query { for f in ctx.Main.File do where (f.Folder = folderId) }
        |> mapFiles

    member this.Find(id) =
        query { for f in ctx.Main.File do where (f.Id = id) }
        |> mapFiles
        |> Seq.tryHead

    member this.FindForUser(user) = 
        query { for f in ctx.Main.File do where (f.User = user) }
        |> mapFiles

    member this.Update(id, name, access, description, user, folder) =
        let file = query { for f in ctx.Main.File do where (f.Id = id) } |> Seq.tryHead
        match file with
        | None -> failwith "File not found"
        | Some(f) ->
            f.Name <- name
            f.Access <- access
            f.Description <- description
            f.Folder <- folder
            f.DateChanged <- DateTime.Now.ToString()
            ctx.SubmitUpdatesAsync() |> Async.StartAsTask

    member this.Delete(id) = 
        let file = query { for f in ctx.Main.File do where (f.Id = id) } |> Seq.tryHead
        match file with
        | None -> failwith "File not found"
        | Some(f) ->
            f.Delete()
            ctx.SubmitUpdatesAsync() |> Async.StartAsTask

    member this.FindWithNameContains(q: string, user) = 
        let upperQuery = q.ToUpper()
        query { for f in ctx.Main.File do where (f.User = user) }
        |> Seq.filter (fun f -> f.Name.ToUpper().Contains(upperQuery))
        |> mapFiles

    member this.FindAll() = 
        ctx.Main.File
        |> mapFiles