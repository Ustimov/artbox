﻿namespace ArtBox.Core.Services

open ArtBox.Core.Gateways
open BCrypt.Net
open System

type UserService() = 

    let userTableDataGateway = UserTableDataGateway()
    let folderTableDataGateway = FolderTableDataGateway()

    member this.Register(email, password, fullName) = 
        match userTableDataGateway.FindByEmail(email) with
        | Some(user) -> (false, "User already exists")
        | None ->
            let hash = BCrypt.HashPassword(password)
            userTableDataGateway.Insert(email, hash, fullName, "") |> ignore
            folderTableDataGateway.Insert("Root", 0L, 1L, "", email) |> ignore
            (true, "Ok")

    member this.Auth(email, password) = 
        match userTableDataGateway.FindByEmail(email) with
        | None -> (false, "User not found")
        | Some(_, hash, _, _) ->
            if BCrypt.Verify(password, hash) then (true, "Ok")
            else (false, "Wrong password")
    
    member this.UpdatePassword(email, oldPassword, newPassword) =
        match userTableDataGateway.FindByEmail(email) with
        | None -> (false, "User not found")
        | Some(_, hash, fullName, photo) ->
            if not (BCrypt.Verify(oldPassword, hash)) then
                (false, "Wrong password")
            else
                let newHash = BCrypt.HashPassword(newPassword)
                userTableDataGateway.Update(email, newHash, fullName, photo) |> ignore
                (true, "Ok")

    member this.UpdatePhoto(email, photo) =
        match userTableDataGateway.FindByEmail(email) with
        | None -> (false, "User not found")
        | Some(_, password, fullName, _) ->
            userTableDataGateway.Update(email, password, fullName, photo) |> ignore
            (true, "Ok")

    member this.GetPhotoPath(email) =
        match userTableDataGateway.FindByEmail(email) with
        | None -> (false, "User not found", "")
        | Some(_, _, _, photo) ->
            if photo = "" then
                (false, "No uploaded photo", "")
            else
                (true, "Ok", photo)

    member this.GetFullName(email) =
        match userTableDataGateway.FindByEmail(email) with
        | None -> (false, "User not found", "")
        | Some(_, _, fullName, _) -> (true, "Ok", fullName)

    member this.UpdateFullName(email, fullName) =
        match userTableDataGateway.FindByEmail(email) with
        | None -> (false, "User not found")
        | Some(_, password, _, photo) ->
            userTableDataGateway.Update(email, password, fullName, photo) |> ignore
            (true, "Ok")

type FolderService() =

    let folderTableDataGateway = FolderTableDataGateway()

    member this.GetUserRootFolder(email) =
        match folderTableDataGateway.FindUserRoot(email) with
        | None -> (false, "Root folder not found", None)
        | Some(folder) -> (true, "Ok", Some(folder))

    member this.GetFolderById(email, id) = 
        let folder = folderTableDataGateway.Find(id)
        match folder with
        | None -> (false, "Folder not found", folder)
        | Some(_, _, _, _, access, _, user) when user <> email && access = 1L -> 
            (false, "This is private folder", folder)
        | Some(_) -> (true, "Ok", folder)

    member this.CreateFolder(name, parent, description, access, email) = 
        match folderTableDataGateway.Find(parent) with
        | None -> (false, "Parent folder not found")
        | Some(_) -> folderTableDataGateway.Insert(name, parent, access, description, email) |> ignore
                     (true, "Ok")

    member this.GetFoldersForUser(user) = 
        folderTableDataGateway.FindForUser(user)

    member this.GetFoldersForParent(parent) = 
        folderTableDataGateway.FindForParent(parent)

    member this.UpdateFolder(email, id, name, access, description) = 
        let folder = folderTableDataGateway.Find(id)
        match folder with
        | None -> (false, "Folder not found")
        | Some(_, _, _, _, _, _, user) when email <> user -> (false, "Only owner can edit folder")
        | Some(_) ->
            folderTableDataGateway.Update(id, name, access, description, email) |> ignore
            (true, "Ok")

    member this.DeleteFolder(email, id) = 
        let folder = folderTableDataGateway.Find(id)
        match folder with
        | None -> (false, "Folder not found")
        | Some(_, _, _, _, _, _, user) when email <> user -> (false, "Only owner can delete folder")
        | Some(_) ->
            folderTableDataGateway.Delete(id) |> ignore
            (true, "Ok")

    member this.ShortSearch(query, user) = 
        folderTableDataGateway.FindWithNameContains(query, user)

type FileService() = 

    let folderTableDataGateway = FolderTableDataGateway()
    let fileTableDataGateway = FileTableDataGateway()

    member this.SaveFile(name, folder, fileType, size, access, description, user, path) =
        match folderTableDataGateway.Find(folder) with
        | None -> (None, "Folder not found")
        | Some(_, _, _, _, _, _, email) when email <> user -> (None, "You haven't access to this folder")
        | Some(f) when fileTableDataGateway.FindForFolder(folder)
                       |> Seq.exists (fun f -> let _, n, _, _, _, _, _, _, _, _, _ = f
                                               name = n)
                       -> (None, "File with this name already exists")
        | Some(f) ->
            let id = fileTableDataGateway.Insert(name, folder, fileType, size, access, description, user, path)
            (Some(id), "Ok")

    member this.GetFileById(email, id) = 
        let file = fileTableDataGateway.Find(id)
        match file with
        | None -> (false, "File not found", file)
        | Some(_, _, _, _, _, _, _, access, _, user, _)
            when access = 1L && user <> email -> (false, "You haven't access to this file", file)
        | Some(f) -> (true, "Ok", file)

    member this.GetFilesForUser(user) = 
        fileTableDataGateway.FindForUser(user)

    member this.GetFilesForFolder(folder) = 
        fileTableDataGateway.FindForFolder(folder)

    member this.UpdateFile(email, id, name, access, description, folder) = 
        let file = fileTableDataGateway.Find(id)
        match file with
        | None -> (false, "File not found")
        | Some(_, _, _, _, _, _, _, _, _, user, _) when email <> user -> (false, "Only owner can edit file")
        | Some(_) ->
            fileTableDataGateway.Update(id, name, access, description, email, folder) |> ignore
            (true, "Ok")

    member this.DeleteFile(email, id) =
        let file = fileTableDataGateway.Find(id)
        match file with
        | None -> (false, "File not found")
        | Some(_, _, _, _, _, _, _, _, _, user, _) when email <> user -> (false, "Only owner can delete file")
        | Some(_) ->
            fileTableDataGateway.Delete(id) |> ignore
            (true, "Ok")

    member this.ShortSearch(query, user) = 
        fileTableDataGateway.FindWithNameContains(query, user)

    member this.FullSearch(email, query: string option, inName: bool option, inDescription: bool option,
                           inType: bool option, dateCreatedFrom: DateTime option, dateCreatedTo: DateTime option,
                           dateChangedFrom: DateTime option, dateChangedTo: DateTime option) =
        let mutable files = fileTableDataGateway.FindAll()
        files <- files
                 |> Seq.filter (fun f ->
                     let id, name, folder, dateCreated, dateChanged, fileType, size, access, description, u, path = f
                     u = email)
        if (inName.IsNone || (inName.IsSome && inName.Value = false)) && (inDescription.IsNone || (inDescription.IsSome && inDescription.Value = false)) && (inType.IsNone || (inType.IsSome && inType.Value = false)) && query.IsSome then
            files <- files
                     |> Seq.filter (fun f ->
                         let id, name, folder, dateCreated, dateChanged, fileType, size, access, description, u, path = f
                         name.Contains(query.Value) || description.Contains(query.Value) || fileType.Contains(query.Value))
        if inName.IsSome && query.IsSome && inName.Value = true then
            files <- files
                     |> Seq.filter (fun f ->
                         let id, name, folder, dateCreated, dateChanged, fileType, size, access, description, u, path = f
                         name.Contains(query.Value))
        if inDescription.IsSome && query.IsSome && inDescription.Value = true then
            files <- files
                     |> Seq.filter (fun f ->
                         let id, name, folder, dateCreated, dateChanged, fileType, size, access, description, u, path = f
                         description.Contains(query.Value))
        if inType.IsSome && query.IsSome && inType.Value = true then
            files <- files
                     |> Seq.filter (fun f ->
                         let id, name, folder, dateCreated, dateChanged, fileType, size, access, description, u, path = f
                         fileType.Contains(query.Value))
        if dateCreatedFrom.IsSome then     
            files <- files
                     |> Seq.filter (fun f ->
                         let id, name, folder, dateCreated, dateChanged, fileType, size, access, description, u, path = f
                         DateTime.Parse(dateCreated) >= dateCreatedFrom.Value)
        if dateCreatedTo.IsSome then     
            files <- files
                     |> Seq.filter (fun f ->
                         let id, name, folder, dateCreated, dateChanged, fileType, size, access, description, u, path = f
                         DateTime.Parse(dateCreated) <= dateCreatedTo.Value)
        if dateChangedFrom.IsSome then     
            files <- files
                     |> Seq.filter (fun f ->
                         let id, name, folder, dateCreated, dateChanged, fileType, size, access, description, u, path = f
                         DateTime.Parse(dateChanged) >= dateChangedFrom.Value)
        if dateChangedTo.IsSome then     
            files <- files
                     |> Seq.filter (fun f ->
                         let _, _, _, _, dateChanged, _, _, _, _, _, _ = f
                         DateTime.Parse(dateChanged) <= dateChangedTo.Value)
        files
