﻿namespace ArtBox.Core

open FSharp.Data.Sql

module internal Database = 
    let [<Literal>] connectionString = "Data Source=" + __SOURCE_DIRECTORY__ + @"\..\sqlite.db;Version=3"
    type Sql = SqlDataProvider<ConnectionString = connectionString, DatabaseVendor = Common.DatabaseProviderTypes.SQLITE>
